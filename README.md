# MetaBERT

Codes for ["MetaBERT: Collaborative Meta-Learning for Accelerating BERT Inference", published in CSCWD 2023](https://www.researchgate.net/publication/371825485_MetaBERT_Collaborative_Meta-Learning_for_Accelerating_BERT_Inference).

## Requirements

We recommend using Anaconda for setting up the environment of experiments:

```bash
conda create -n metabert python=3.8.8
conda activate metabert
conda install pytorch==1.8.1 cudatoolkit=11.1 -c pytorch -c conda-forge
pip install -r requirements.txt
```

## Downstream task datasets

The GLUE task datasets can be downloaded from the [**GLUE leaderboard**](https://gluebenchmark.com/tasks).

**Please see our paper for more details!**

## Contact

If you have any problems, raise an issue or contact [Yangyan Xu](mailto:yangyanxu0802@foxmail.com).

## Citation

If you find this repo helpful, we'd appreciate it a lot if you can cite the corresponding paper:

```
@inproceedings{xu2023metabert,
  title={MetaBERT: Collaborative Meta-Learning for Accelerating BERT Inference},
  author={Xu, Yangyan and Yuan, Fangfang and Cao, Cong and Zhang, Xiaoliang and Su, Majing and Wang, Dakui and Liu, Yanbing},
  booktitle={2023 26th International Conference on Computer Supported Cooperative Work in Design (CSCWD)},
  pages={119--124},
  year={2023},
  organization={IEEE}
}
```